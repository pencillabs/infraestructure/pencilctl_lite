# pencilctl-lite

Repositório contendo o pacote python `pencilctl-lite`, responsável por auxiliar o deploy de projetos que utilizam o modo Swarm do Docker. O objetivo do projeto é que, a partir de uma estrutura de diretórios padronizada, seja possível construir imagens e disponibilizar serviços de forma simples e intuitiva.

## Quick Start

Para instalar o pacote, execute o comando:

	pip install pencilctl-lite

> O projeto depende que a versão mínima do python na máquina seja **3.7**.

Com o pacote instalado, existem dois pré-requisitos para iniciarmos o deploy dos ambientes:

1. Um cluster Swarm tem que existir na máquina em que a ferramenta será executada ou a máquina faz parte de um cluster e atua como `manager`.
2. O projeto que será disponibilizado no cluster precisa possuir uma pasta docker com a seguinte estrutura:

![estrutura ](./estrutura.png)

O pencilctl-lite irá utilizar como ambiente padrão para o deploy o diretório `docker/production`, mas outros diretórios podem ser criados. Cada diretório dentro da pasta `docker` pode ser utilizado para representar um ambiente a ser disponibilizado dentro do cluster. Por exemplo: é possível ter uma pasta chamada `local-swarm` para realizar o deploy do projeto em um cluster local e, na pasta production, manter os arquivos para o deploy em produção.

A partir da estrutura da imagem anterior, é possível gerar a imagem dos serviços com os comandos:

	python -m pencilctl-lite --command build --service server
	python -m pencilctl-lite --command build --service nginx

> A imagem resultante do comando build será "tageda" com o mesmo nome definido no arquivo `stack.yml`.

Para fazer deploy do arquivo `stack.yml`, utilize o comando:

	python -m pencilctl-lite --command deploy --stack nomedesejado


É possível alterar qual ambiente será lido pela ferramenta criando o arquivo `.config.toml` na raiz do repositório. Esse arquivo tem o seguinte conteúdo:

```toml
registry="127.0.0.1:5000"
branch="stable"
service="server"
docker_dir = "docker"
env="local-swarm"
docker_path=""
stack=""
```


Para saber quais comandos e argumentos a ferramenta possui, execute o comando:

    python -m pencilctl-lite -h
