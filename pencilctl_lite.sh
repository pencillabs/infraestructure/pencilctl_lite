#!/bin/bash
# This script is a minimal implementation of pencilctl binary.
# It is usefull to deploy stacks on swarm on infraestructure that does not rely on Pencil main cluster.

env=prod
registry=127.0.0.1:5000
branch=stable
service=server
docker_path=
stack=

# Extracts command line arguments
for arg in "$@" ; do
branch_arg=$(echo $arg | grep "branch")
stack_arg=$(echo $arg | grep "stack")
docker_path_arg=$(echo $arg | grep "docker_path")
service_arg=$(echo $arg | grep "service")
case "$arg" in
	$branch_arg)
	branch=$(echo $arg | sed "s#.*=##")
	;;
	$stack_arg)
	stack=$(echo $arg | sed "s#.*=##")
	;;
	$docker_path_arg)
	docker_path=$(echo $arg | sed "s#.*=##")
	;;
	$service_arg)
	service=$(echo $arg | sed "s#.*=##")
	esac
done

help() {
	echo Uso: make [opções] comando
	echo Opções:
	echo "env=<env>, default: prod    Informa qual ambiente deve ser utilizado para construir as imagens e fazer deploy da stack."
	echo "stack=<stack>, default:    Informa qual stack deve ser usada para construir as imagens e fazer deploy do projeto."
	echo "branch=<branch>, default: stable  Informa qual branch deve ser usada para clonar o repositório do projeto, na fase de construção da imagem"
	echo "docker_path=<path>, default: .  Informa o caminho absoluto para a raiz do diretório contendo os arquivos Dockerfile e *-stack.yml."
	echo "service=<service>, default: server  Informa qual serviço da stack deve ser utilizado para constuir a imagem Docker"
	echo ""
	echo Comandos:
	echo "build: Constroi as imagens Docker, presentes no caminho <docker_path>/<service>/Dockerfile-<env>"
	echo "deploy: Faz deploy de uma stack no caminho <docker_path>/stacks/<env>-<stack>-stack.yml"
	echo ""
	echo "Exemplo de build: ./pencilctl_lite.sh build --docker_path=/home/davidcarlos/projects/pencillabs/ng/ngcredits/docker --service=server --stack=ng" 
	echo "Exemplo de deploy: ./pencilctl_lite.sh deploy --docker_path=/home/davidcarlos/projects/pencillabs/ng/ngcredits/docker --stack=ng" 
}

build() {
	if [[ -z $stack ]]; then 
	echo variável stack não foi informada. Para uma lista completa de opções,  execute o comando help.
	return
	fi
	if [[ -z $docker_path ]]; then 
	echo variável docker_path não foi informada. Para uma lista completa de opções,  execute o comando help.
	return
	fi
	image_name="$stack"_"$service"
	docker build --build-arg branch=$branch -t $registry/$image_name:$branch-$env -f $docker_path/production/$stack/$service/Dockerfile-$env $docker_path/production  
}

deploy() {
	if [[ -z $stack ]]; then 
	echo variável stack não foi informada. Para uma lista completa de opções,  execute o comando help.
	return
	fi
	if [[ -z $docker_path ]]; then 
	echo variável docker_path não foi informada. Para uma lista completa de opções,  execute o comando help.
	return
	fi
	registry=$registry docker stack deploy -c $docker_path/production/$stack/stacks/$env-$stack-stack.yml "$stack"_"$env"
}

case $1 in
	help)
		help
		;;
	build)
		build
		;;
	deploy)
		deploy
esac

